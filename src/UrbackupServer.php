<?php
namespace OSSystems\Urbackup;

use Requests;

/**
 * PHP Urbackup
 * 
 * A class for interacting with a Urbackup server.
 *
 * @category   PHP
 * @package    OSS\Urbackup
 * @author     Ashley Hood <ashley@opensauce.systems>
 * @copyright  2010-2015 Open Sauce Systems
 * @version    $Format:%H$
 * @since      Aug 7 15:00:00 UTC 2015
 */
class UrbackupServer
{
    private $username;
    private $password;
    private $url;
    private $port = 55414;
    private $session;
    private $loggedIn = false;

    public $message;

    public function __construct($username, $password, $url, $port = null)
    {
        $this->username = $username;
        $this->password = $password;
        $this->url = $url;

        if (!empty($port)) {
            $this->port = $port;
        }
    }

    /**
     * Send request to Urbackup server and get response
     *
     * @param string $action   action to get data for
     * @param array $params   additional parameters
     * @return array
     */
    private function getRequest($action, Array $params = [])
    {
        $serverUrl = $this->url.':'.$this->port."/x?".http_build_query(['a' => $action]);

        if (!empty($this->session)) {
            $params["ses"] = $this->session;
        }

        $serverUrl .= '&'.http_build_query($params);

        $headers = [];

        $options = [
            'timeout' => 100,
        ];

        $response = Requests::get($serverUrl, $headers, $options);

        return json_decode($response->body);
    }

    /**
     * Log into Urbackup server
     *
     * @return bool
     */
    public function login()
    {
        if ($this->loggedIn) {
            return true;
        }

        $salt = $this->getRequest('salt', ['username' => $this->username]);

        // Username does not exist
        if (empty($salt) || !isset($salt->ses)) {
            $this->message = 'Username does not exist';
            return false;
        }

        $this->session = $salt->ses;

        if (isset($salt->salt)) {
            $passwordMd5 = md5($salt->rnd.md5($salt->salt.$this->password));

            $login = $this->getRequest(
                'login',
                ['username' => $this->username, 'password' => $passwordMd5]
            );

            if (empty($login) || !$login->success) {
                $this->message = 'Error during login. Password wrong?';
                return false;
            } else {
                $this->loggedIn = true;
                return true;
            }
        } else {
            return false;
        }
    }

    /**
     * Get all Urbackup clients
     *
     * @param string $clientname   name of client
     * @return mixed
     */
    public function getClients()
    {
        if (!$this->login()) {
            return false;
        }

        $clients = $this->getRequest('status');

        if (empty($clients) || !isset($clients->client_downloads)) {
            $this->message = 'Could not get clients.';
            return false;
        }

        return $clients->client_downloads;
    }

    /**
     * Get ID of a Urbackup client
     *
     * @param string $clientname   name of client
     * @return mixed
     */
    public function getClientId($clientname)
    {
    	if (!$this->login()) {
            return false;
        }

        $status = $this->getRequest('status');

        if (empty($status)) {
            return false;
        }

        if (empty($status->client_downloads)) {
        	return false;
        }

        foreach ($status->client_downloads as $client) {
            if ($client->name == $clientname) {
                if (!empty($client->id)) {
                    return $client->id;
                }
            }
        }

        $this->message = 'Could not find client info.';
        return false;
    }

    /**
     * Create a new internet client on the Urbackup server
     *
     * @param string $clientname   name of client to backup
     * @return mixed
     */
    public function createInternetClient($clientname)
    {
        if (!$this->login()) {
            return false;
        }

        $result = $this->getRequest(
            'status',
            ['clientname' => $clientname]
        );

        return !empty($result);
    }

    /**
     * Get the settings for a Urbackup client
     *
     * @param string $clientname   name of client
     * @return mixed
     */
    public function getClientSettings($clientname)
    {
        if (!$this->login()) {
            return false;
        }

        $clientId = $this->getClientId($clientname);

        if (empty($clientId)) {
        	return false;
        }

        $params = [
        	'sa' => 'clientsettings',
        	't_clientid' => $clientId
        ];

        $settings = $this->getRequest('settings', $params);

        if (empty($settings) || !isset($settings->settings)) {
        	$message = 'Could not get client settings.';
        	return false;
        }

        return $settings->settings;
    }

    /**
     * Get the internet authentication key
     * used to authenticate client with the server
     *
     * @param string $clientname   name of client
     * @return mixed
     */
    public function getClientAuthkey($clientname)
    {
        if (!$this->login()) {
            return false;
        }

        $settings = $this->getClientSettings($clientname);

        if (empty($settings)) {
        	return false;
        }

        return $settings->internet_authkey;
    }

    /**
     * Get the general settings for Urbackup
     *
     * @return mixed
     */
    public function getSettings()
    {
        if (!$this->login()) {
            return false;
        }

        $settings = $this->getRequest('settings');

        if (empty($settings) || !isset($settings->settings)) {
            $this->message = 'Could not get settings.';
            return false;
        }

        return $settings->settings;
    }

    /**
     * Get the status of all the clients on the Urbackup server
     *
     * @return mixed
     */
    public function getStatus()
    {
        if (!$this->login()) {
            return false;
        }

        $status = $this->getRequest('status');

        if (empty($status)) {
            return null;
        }

        if (isset($status->status)) {
            return [
        	    'clients' => $status->client_downloads,
        	    'status' => $status->status
            ];
        }

        return null;
    }
    
    /**
     * Get the progress of any backups being done
     *
     * If pcdone < 0 then the client is indexing the files to backup.
     * Otherwise pcdone is the percentage of the backup completed
     *
     * @return mixed
     */
    public function getProgress()
    {
        if (!$this->login()) {
            return false;
        }
        
        $progress = $this->getRequest('progress');
        
        if (empty($progress)) {
            return null;
        }
        
        if (isset($progress->progress)) {
            return $progress->progress;
        }
        
        return null;
    }
    
    /**
     * Get a list of the last backup activities
     *
     * @return mixed
     */
    public function getLastActivities()
    {
        if (!$this->login()) {
            return false;
        }
        
        $lastActivities = $this->getRequest('progress');
        
        if (empty($lastActivities)) {
            return null;
        }
        
        if (isset($lastActivities->lastacts)) {
            return $lastActivities->lastacts;
        }
        
        return null;
    }

    /**
     * Get the client usage statistics
     *
     * @return mixed
     */
    public function getUsage()
    {
        if (!$this->login()) {
            return false;
        }
        
        $usage = $this->getRequest('usage');
        
        if (empty($usage)) {
            return null;
        }
        
        if (isset($usage->usage)) {
            return $usage->usage;
        }
        
        return null;
    }
    
    /**
     * Get the client usage graph
     *
     * @return mixed
     */
    public function getUsageGraph()
    {
        if (!$this->login()) {
            return false;
        }
        
        $usageGraph = $this->getRequest('usagegraph');
        
        if (empty($usageGraph)) {
            return null;
        }
        
        return $usageGraph;
    }

    /**
     * Start a given type of backup of a client.
     *
     * @param string $clientname   name of client to backup
     * @param string $type   type of backup to preform
     * @return bool
     */
    private function startBackup($clientname, $type)
    {
     	$clientId = $this->getClientId($clientname);

     	if (empty($clientId)) {
     		return false;
     	}

     	$params = [
     		'start_client' => $clientId,
     		'start_type' => $type
     	];

     	$result = $this->getRequest('status', $params);

     	if (empty($result)) {
     		return false;
     	}

     	return true;
    }

    /**
     * Start an incremental file backup
     *
     * @param string $clientname   name of client to backup
     * @return bool
     */
    public function startIncrementalFileBackup($clientname)
    {
        return $this->startBackup($clientname, 'incr_file');
    }

    /**
     * Start a full file backup
     *
     * @param string $clientname   name of client to backup
     * @return bool
     */
    public function startFullFileBackup($clientname)
    {
        return $this->startBackup($clientname, 'full_file');
    }

    /**
     * Start an incremental image backup
     *
     * @param string $clientname   name of client to backup
     * @return bool
     */
    public function startIncrementalImageBackup($clientname)
    {
        return $this->startBackup($clientname, 'incr_image');
    }

    /**
     * Start a full image backup
     *
     * @param string $clientname   name of client to backup
     * @return bool
     */
    public function startFullImageBackup($clientname)
    {
        return $this->startBackup($clientname, 'full_image');
    }

}
