<?php
namespace OSSystems\Urbackup;

use OSSystems\Urbackup\UrbackupServer;

/**
 * PHP Urbackup
 *
 * A class for interacting with a Urbackup server.
 *
 * @category   PHP
 * @package    OSS\Urbackup
 * @author     Ashley Hood <ashley@opensauce.systems>
 * @copyright  2010-2015 Open Sauce Systems
 * @version    $Format:%H$
 * @since      Aug 7 15:00:00 UTC 2015
 */
class UrbackupHelper extends UrbackupServer
{
    /**
     * Get the client usage as on disk
     *
     * @return mixed
     */
    public function getDiskUsage()
    {
        if (!$this->login()) {
            return false;
        }

        $clients = $this->getClients();
        $settings = $this->getSettings();

        if (empty($clients) || empty($settings)) {
            $this->message = 'Could not get clients or settings.';
            return false;
        }

        $backupDir = $settings->backupfolder;
        $clientsDiskUsage = [];

        foreach ($clients as $client) {
            $cmd = 'du -s '. $backupDir.'/'.$client->name;
            $du = exec($cmd, $output);
            $du = explode("/", $du);
            $du = trim($du[0]);
            $client->diskusage = $du*1024;
            $clientsDiskUsage[] = $client;
        }

        return $clientsDiskUsage;
    }
}
