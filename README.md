# Urbackup Server API Helper #

Is a helper class for the calling Urbackup API...

Add the following to your projects composer to bring in the code:

    {
        "require": {
            "oss/Urbackup": "dev-master"
        },
        "repositories": [
            {
                "type": "vcs",
                "url": "git@bitbucket.org:opensaucesystemsdevelopment/urbackup-api.git"
            }
        ]
    }


###Usage###

    <?php
    require 'vendor/autoload.php';

    use OSS\Urbackup\UrbackupServer;

    $username = 'admin';
    # Password is same as lake
    $password = '';
    $url = 'http://binstead.opensaucesystems.com';

    $urbackup = new UrbackupServer($username, $password, $url);

    $status = $urbackup->getStatus();

    print_r($status);

    //$result          = $urbackup->createInternetClient('oss-test');
    
    //$settings        = $urbackup->getClientSettings('oss-elmfield');
    
    //$internetAuthKey = $urbackup->getClientAuthkey('oss-elmfield');
    
    //$result          = $urbackup->startIncrementalFileBackup('oss-elmfield');

    //$result          = $urbackup->startFullFileBackup('oss-elmfield');

    //$progress        = $urbackup->getProgress();

    //$lastActivities  = $urbackup->getLastActivities();